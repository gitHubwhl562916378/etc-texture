#version 330
uniform mat4 uPMatrix,uVMatrix,uMMatrix;
layout (location = 0) in vec3 aPositin;
layout (location = 1) in vec2 aTextureCoord;
smooth out vec2 vTextureCoord;

void main(void)
{
    gl_Position = uPMatrix * uVMatrix * uMMatrix * vec4(aPositin,1);
    vTextureCoord = aTextureCoord;
}
