#include <QFile>
#include "etcrender.h"

void EtcRender::initsize(QString pkmFilePath)
{
    program_.addCacheableShaderFromSourceFile(QOpenGLShader::Vertex,"vsrc.vert");
    program_.addCacheableShaderFromSourceFile(QOpenGLShader::Fragment,"fsrc.frag");
    program_.link();

    texture_ = new QOpenGLTexture(QOpenGLTexture::Target2D);
    texture_->create();
    QFile file(pkmFilePath);
    if(file.open(QIODevice::ReadOnly)){
        QByteArray arr = file.readAll();
        texture_->setCompressedData(arr.size(),arr.data());
    }
    GLfloat points[]{
    -1.0,-1.0,0.0,
    -1.0,+1.0,0.0,
    +1.0,+1.0,0.0,
    +1.0,-1.0,0.0,

    0.0,+1.0,
    0.0,0.0,
    +1.0,0.0,
    +1.0,+1.0
    };
    vbo_.create();
    vbo_.bind();
    vbo_.allocate(points,sizeof(points));
}

void EtcRender::render(QOpenGLExtraFunctions *f, QMatrix4x4 &pMatrix, QMatrix4x4 &vMatrix, QMatrix4x4 &mMatrix)
{
    f->glDisable(GL_CULL_FACE);
    f->glDisable(GL_DEPTH_TEST);

    program_.bind();
    vbo_.bind();
    f->glActiveTexture(GL_TEXTURE0 + 0);
    program_.setUniformValue("sTextures",0);
    program_.setUniformValue("uPMatrix",pMatrix);
    program_.setUniformValue("uVMatrix",vMatrix);
    program_.setUniformValue("uMMatrix",mMatrix);
    program_.enableAttributeArray(0);
    program_.enableAttributeArray(1);
    program_.setAttributeBuffer(0,GL_FLOAT,0,3,3 * sizeof(GLfloat));
    program_.setAttributeBuffer(1,GL_FLOAT,3 * 4 * sizeof(GLfloat),2,2 *sizeof(GLfloat));

    texture_->bind(0);
    f->glDrawArrays(GL_TRIANGLE_FAN,0,4);

    program_.disableAttributeArray(0);
    program_.disableAttributeArray(1);
    texture_->release();
    vbo_.release();
    program_.release();
}
