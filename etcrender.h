#ifndef ETCRENDER_H
#define ETCRENDER_H

#include <QOpenGLExtraFunctions>
#include <QOpenGLShaderProgram>
#include <QOpenGLBuffer>
#include <QOpenGLTexture>
class EtcRender
{
public:
    EtcRender() = default;
    void initsize(QString pkmFilePath);
    void render(QOpenGLExtraFunctions *f, QMatrix4x4 &pMatrix,QMatrix4x4 &vMatrix,QMatrix4x4 &mMatrix);

private:
    QOpenGLShaderProgram program_;
    QOpenGLBuffer vbo_;
    QOpenGLTexture *texture_{nullptr};
};

#endif // ETCRENDER_H
