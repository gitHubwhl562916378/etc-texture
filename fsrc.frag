#version 330
uniform sampler2D sTextures;
in vec2 vTextureCoord;
out vec4 fragColor;

void main(void)
{
    fragColor = texture2D(sTextures,vTextureCoord);
}
